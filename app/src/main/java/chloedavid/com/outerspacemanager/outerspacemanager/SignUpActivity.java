package chloedavid.com.outerspacemanager.outerspacemanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener  {

    private TextView textView_identifiant;
    private TextView textView_mdp;
    private TextView textView_email;
    private EditText editText_identifiant;
    private EditText editText_mdp;
    private EditText editText_email;
    private Button signUp;
    private Button signIn;

    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        textView_identifiant = (TextView) findViewById(R.id.textView_identifiant_id);
        textView_mdp = (TextView) findViewById(R.id.textView_mdp_id);
        textView_email = (TextView) findViewById(R.id.textView_email_id);
        editText_identifiant  = (EditText) findViewById(R.id.editText_identifiant_id);
        editText_mdp  = (EditText) findViewById(R.id.editText_mpd_id);
        editText_email = (EditText) findViewById(R.id.editText_email_id);
        signUp = (Button) findViewById(R.id.button_sign_up_id);
        signIn = (Button) findViewById(R.id.button_toSignIn_id);

        signUp.setOnClickListener(this);
        signIn.setOnClickListener(this);


        SharedPreferences settings = getSharedPreferences(SignUpActivity.PREFS_NAME, 0);
        String token = settings.getString("Token", "Error");

        if (token != "Error") {
            Intent myIntent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(myIntent);
        }

    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.button_sign_up_id) {

            String username = editText_identifiant.getText().toString();
            String email = editText_email.getText().toString();
            String password = editText_mdp.getText().toString();

            RequestCreateUser info = new RequestCreateUser(username, email, password);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://outer-space-manager-staging.herokuapp.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            GitHubService service = retrofit.create(GitHubService.class);
            Call<ResponseCreateUser> responseCreateUserCall = service.createUser(info);

            responseCreateUserCall.enqueue(new Callback<ResponseCreateUser>() {
                @Override
                public void onResponse(Call<ResponseCreateUser> call, Response<ResponseCreateUser> response) {
                    if (response.isSuccessful()) {

                        // We need an Editor object to make preference changes.
                        // All objects are from android.context.Context
                        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("Token", response.body().token);

                        // Commit the edits!
                        editor.commit();

                        // Go to new activity
                        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(myIntent);

                    } else {
                        String test = null;
                        try {
                            test = response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(view.getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseCreateUser> call, Throwable t) {

                }
            });
        } else {
            // Go to new activity
            Intent myIntent = new Intent(getApplicationContext(), SignInActivity.class);
            startActivity(myIntent);
        }
    }

}
