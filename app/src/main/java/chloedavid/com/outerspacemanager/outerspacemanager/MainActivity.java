package chloedavid.com.outerspacemanager.outerspacemanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chlo on 18/04/2018.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textViewToken;
    private Button signOut;
    private Button batiments;
    private TextView gas;
    private TextView minerals;
    private TextView points;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences settings = getSharedPreferences(SignUpActivity.PREFS_NAME, 0);
        String token = settings.getString("Token", "default");

        textViewToken = (TextView) findViewById(R.id.textViewToken_id);
        signOut = (Button) findViewById(R.id.button_sign_out_id);
        batiments = (Button) findViewById(R.id.button_batiments_id);
        gas = (TextView) findViewById(R.id.textViewGas_id);
        points = (TextView) findViewById(R.id.textViewPoint_id);
        minerals = (TextView) findViewById(R.id.textViewMineral_id);

        signOut.setOnClickListener(this);
        batiments.setOnClickListener(this);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://outer-space-manager-staging.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GitHubService service = retrofit.create(GitHubService.class);
        Call<ResponseUser> responseUserCall = service.getUser(token);

        responseUserCall.enqueue(new Callback<ResponseUser>() {

            @Override
            public void onResponse(Call<ResponseUser> call, Response<ResponseUser> response) {
                if (response.isSuccessful()){

                    textViewToken.setText(response.body().username);

                    points.setText( String.valueOf(response.body().points));
                    gas.setText( String.valueOf(response.body().gas));
                    minerals.setText( String.valueOf(response.body().minerals));

                }else{
                    String test = null;
                    try {
                        test = response.errorBody().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //Toast.makeText(MainActivity.getContext(), "Error", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseUser> call, Throwable t) {

            }

        });
    }


    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.button_sign_out_id){
            SharedPreferences settings = getSharedPreferences(SignUpActivity.PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.remove("Token");

            // Commit the edits!
            editor.commit();

            // Go to new activity
            Intent myIntent = new Intent(getApplicationContext(),SignInActivity.class);
            startActivity(myIntent);

        } else if(view.getId() == R.id.button_batiments_id) {


            // Go to new activity
            Intent myIntent = new Intent(getApplicationContext(),BuildingActivity.class);
            startActivity(myIntent);
        }

    }

}
