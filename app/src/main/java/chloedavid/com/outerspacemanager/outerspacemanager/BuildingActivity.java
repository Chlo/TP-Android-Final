package chloedavid.com.outerspacemanager.outerspacemanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chlo on 18/04/2018.
 */

public class BuildingActivity extends AppCompatActivity{

    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://outer-space-manager-staging.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GitHubService service = retrofit.create(GitHubService.class);

        SharedPreferences settings = getSharedPreferences(SignUpActivity.PREFS_NAME, 0);
        String token = settings.getString("Token", "default");
        Call<ResponseGetBuilding> responseGetBuildingCall = service.getBuilding(token);

        responseGetBuildingCall.enqueue(new Callback<ResponseGetBuilding>() {
            @Override
            public void onResponse(Call<ResponseGetBuilding> call, Response<ResponseGetBuilding> response) {
                if (response.isSuccessful()) {

                    // Create the adapter to convert the array to views
                    ListAdapter adapter = new ListAdapter(BuildingActivity.this, response.body().buildings);
                    // Attach the adapter to a ListView
                    ListView listView = findViewById(R.id.listViewBuilding);
                    listView.setAdapter(adapter);

                }
            }

            @Override
            public void onFailure(Call<ResponseGetBuilding> call, Throwable t) {

            }
        });
    }
}
