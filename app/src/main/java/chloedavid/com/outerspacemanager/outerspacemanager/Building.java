package chloedavid.com.outerspacemanager.outerspacemanager;

/**
 * Created by chlo on 23/05/2018.
 */

public class Building {
    int level;
    int amountOfEffectByLevel;
    int amountOfEffectLevel0;
    int buildingId;
    boolean building;
    String effect;
    @Override
    public String toString() {
        return "MyBuilding{" +
            "level=" + level +
            ", amountOfEffectByLevel=" + amountOfEffectByLevel +
            ", amountOfEffectLevel0=" + amountOfEffectLevel0 +
            ", buildingId=" + buildingId +
            ", building=" + building +
            ", effect='" + effect + '\'' +
            ", gasCostByLevel=" + gasCostByLevel +
            ", gasCostLevel0=" + gasCostLevel0 +
            ", imageUrl='" + imageUrl + '\'' +
            ", mineralCostByLevel=" + mineralCostByLevel +
            ", mineralCostLevel0=" + mineralCostLevel0 +
            ", name='" + name + '\'' +
            ", timeToBuildByLevel=" + timeToBuildByLevel +
            ", timeToBuildLevel0=" + timeToBuildLevel0 +
            '}';
    }
    int gasCostByLevel;
    int gasCostLevel0;
    String imageUrl;
    int mineralCostByLevel;
    int mineralCostLevel0;
    String name;
    int timeToBuildByLevel;
    int timeToBuildLevel0;

    // Constructor that is used to create an instance of the Movie object
    public Building(int level, String name, int mineralCostByLevel, int mineralCostLevel0, int gasCostByLevel, int gasCostLevel0, String imageUrl) {
        this.level = level;
        this.name = name;
        this.mineralCostByLevel = mineralCostByLevel;
        this.mineralCostLevel0 = mineralCostLevel0;
        this.gasCostByLevel = gasCostByLevel;
        this.gasCostLevel0 = gasCostLevel0;
        this.imageUrl = imageUrl;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public int getLevel(){
        return level;
    }
    public void setLevel(int level){
        this.level = level;
    }

    public int getMineralCostByLevel(){
        return mineralCostByLevel;
    }
    public void setMineralCostByLevel(int mineralCostByLevel){
        this.mineralCostByLevel = mineralCostByLevel;
    }

    public int getMineralCostLevel0(){
        return mineralCostLevel0;
    }
    public void setMineralCostLevel0(int mineralCostLevel0){
        this.mineralCostLevel0 = mineralCostLevel0;
    }

    public int getGasCostByLevel(){
        return mineralCostByLevel;
    }
    public void setGasCostByLevel(int gasCostByLevel){
        this.gasCostByLevel = gasCostByLevel;
    }

    public int getGasCostLevel0(){
        return gasCostLevel0;
    }
    public void setGasCostLevel0(int gasCostLevel0){
        this.gasCostLevel0 = gasCostLevel0;
    }

    public String getImageUrl(){
        return imageUrl;
    }
    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }


}
