/**
 * Created by chlo on 23/05/2018.
 */

package chloedavid.com.outerspacemanager.outerspacemanager;

import android.app.AlertDialog;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import junit.framework.Test;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static android.app.PendingIntent.getActivity;

public class ListAdapter extends ArrayAdapter<Building> implements View.OnClickListener  {


    private Context myContext;
    private List<Building> myBuildings = new ArrayList<>();

    public ListAdapter(Context context, ArrayList<Building> buildings) {
        super(context, 0, buildings);
        myContext = context;
        myBuildings = buildings;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        // Check if an existing view is being reused, otherwise inflate the view
        if (listItem == null) {
            listItem = LayoutInflater.from(myContext).inflate(R.layout.adapter,parent,false);
        }

        // Get the data item for this position
        Building currentBuilding = myBuildings.get(position);

        ImageView image = listItem.findViewById(R.id.imageBuilding);
        Glide.with(myContext).load(currentBuilding.getImageUrl()).into(image);

        TextView name = listItem.findViewById(R.id.buildingName);
        name.setText(currentBuilding.getName());

        TextView level = listItem.findViewById(R.id.buildingLevel);
        level.setText(String.valueOf(currentBuilding.getLevel()));

        TextView gas = listItem.findViewById(R.id.buildingGas);
        int totalGas = currentBuilding.getGasCostLevel0() + currentBuilding.getGasCostByLevel() * currentBuilding.getLevel();
        gas.setText(String.valueOf(totalGas));

        TextView min = listItem.findViewById(R.id.buildingMin);
        int totalMin = currentBuilding.getMineralCostLevel0() + currentBuilding.getMineralCostByLevel() * currentBuilding.getMineralCostByLevel();
        min.setText(String.valueOf(totalMin));

        Button increase = listItem.findViewById(R.id.createBuildingButton);
        increase.setOnClickListener(this);

        return listItem;
    }

    @Override
    public void onClick(View v) {

       // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage("Voulez vous augmenter le niveau de")
                .setTitle(R.string.name);

        builder.setPositiveButton("OUI", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
            }
        });
        builder.setNegativeButton("NON", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                Intent myIntent = new Intent(getApplicationContext(),BuildingActivity.class);
                startActivity(myIntent);
            }
        });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

    }
}
