package chloedavid.com.outerspacemanager.outerspacemanager;

/**
 * Created by chlo on 18/04/2018.
 */

class RequestConnectUser {

    String password;
    String username;

    public RequestConnectUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
