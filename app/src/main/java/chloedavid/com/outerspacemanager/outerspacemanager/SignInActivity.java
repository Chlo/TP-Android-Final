package chloedavid.com.outerspacemanager.outerspacemanager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by chlo on 18/04/2018.
 */

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView textView_identifiant;
    private TextView textView_mdp;
    private EditText editText_identifiant;
    private EditText editText_mdp;
    private Button signUp;
    private Button signIn;

    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        textView_identifiant = (TextView) findViewById(R.id.textView_identifiant_id);
        textView_mdp = (TextView) findViewById(R.id.textView_mdp_id);
        editText_identifiant  = (EditText) findViewById(R.id.editText_identifiant_id);
        editText_mdp  = (EditText) findViewById(R.id.editText_mpd_id);
        signIn = (Button) findViewById(R.id.button_sign_in_id);
        signUp = (Button) findViewById(R.id.button_toSignUp_id);

        signIn.setOnClickListener(this);
        signUp.setOnClickListener(this);


        SharedPreferences settings = getSharedPreferences(SignUpActivity.PREFS_NAME, 0);
        String token = settings.getString("Token", "Error");

        if (token != "Error") {
            Intent myIntent = new Intent(getApplicationContext(),MainActivity.class);
            startActivity(myIntent);
        }

    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.button_sign_in_id) {

            String username = editText_identifiant.getText().toString();
            String password = editText_mdp.getText().toString();

            RequestConnectUser info = new RequestConnectUser(username, password);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://outer-space-manager-staging.herokuapp.com")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            GitHubService service = retrofit.create(GitHubService.class);
            Call<ResponseConnectUser> responseConnectUserCall = service.connectUser(info);

            responseConnectUserCall.enqueue(new Callback<ResponseConnectUser>() {
                @Override
                public void onResponse(Call<ResponseConnectUser> call, Response<ResponseConnectUser> response) {
                    if (response.isSuccessful()) {

                        // We need an Editor object to make preference changes.
                        // All objects are from android.context.Context
                        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("Token", response.body().token);

                        // Commit the edits!
                        editor.commit();

                        // Go to new activity
                        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(myIntent);

                    } else {
                        String test = null;
                        try {
                            test = response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(view.getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseConnectUser> call, Throwable t) {

                }
            });
        } else {
            // Go to new activity
            Intent myIntent = new Intent(getApplicationContext(), SignUpActivity.class);
            startActivity(myIntent);
        }
    }

}
