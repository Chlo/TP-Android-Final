package chloedavid.com.outerspacemanager.outerspacemanager;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface GitHubService {

    @POST("/api/v1/auth/create")
    Call<ResponseCreateUser> createUser(@Body RequestCreateUser createUser);

    @GET("/api/v1/users/get")
    Call<ResponseUser> getUser(@Header("x-access-token") String token);

    @POST("/api/v1/auth/login")
    Call<ResponseConnectUser> connectUser(@Body RequestConnectUser connectUser);

    @POST("/api/V1/auth/create/:buildingId")
    Call<ResponseCreateBuilding> createBuilding(@Body RequestCreateBuilding createBuilding);

    @GET("/api/V1/buildings/list")
    Call<ResponseGetBuilding> getBuilding(@Header("x-access-token") String token);

}