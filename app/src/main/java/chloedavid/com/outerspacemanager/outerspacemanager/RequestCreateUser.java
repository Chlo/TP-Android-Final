package chloedavid.com.outerspacemanager.outerspacemanager;

/**
 * Created by chlo on 27/03/2018.
 */

class RequestCreateUser {

    String email;
    String password;
    String username;

    public RequestCreateUser(String username, String email, String password) {
        this.email = email;
        this.username = username;
        this.password = password;
    }

}
